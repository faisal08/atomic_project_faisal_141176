<?php
require_once("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;

$objBookTitle  =  new BookTitle();
$objBookTitle->setData($_GET);
$oneData= $objBookTitle->view("obj");
?>
<div class="modal fade" id="myModalnorm" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h3>Book Record
                    <i class="fa fa-book fa-lg " aria-hidden="true"></i>
                </h3>

                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <table class="table table-responsive">

                    <thead>
                    <tr class="bg-primary">
                        <th>ID</th>
                        <th>Book Title</th>
                        <th>Author Name</th>

                    </tr>
                    </thead>
                    <?php
                    echo "<tbody>";
                    echo "<tr>";
                    echo "<td>".$oneData->id."</td>";
                    echo "<td>".$oneData->booktitle."</td>";
                    echo "<td>".$oneData->author_name."</td>";
                    echo "</tr>";
                    echo "</tbody>";

                    ?>
                </table>
            </div>
            <div class="modal-footer">
                <a class="btn" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
