<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Summary_Of_Organization\Summary_Of_Organization;
$serial=1;
$objSummary=new Summary_Of_Organization();
$alldata=$objSummary->index("obj");
$recordCount= count($alldata);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objSummary->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;
?>

<!DOCTYPE html>
<html lang="en">

<head><title>Atomic Project </title>
    <link href="../../../style/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../bootstrap/js/bootstrap.min.js">
    <script src="../../../bootstrap/js/jquery.min.js"></script>
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../JS/organization.js"></script>
    <script type="text/javascript" src="../../../bootstrap/js/bootstrapValidator.js"></script>
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrapValidator.css"/>
    <link rel="stylesheet" href="../../../font-awesome-4.7.0/css/font-awesome.min.css"/>
    <script type="text/javascript">
        function load(thediv,thefile) {
            if (window.XMLHttpRequest){
                xmlhttp=new XMLHttpRequest();
            }
            else {
                xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
            }
            xmlhttp.onreadystatechange=function () {
                if (xmlhttp.readyState==4 && xmlhttp.status==200){
                    document.getElementById(thediv).innerHTML=xmlhttp.responseText;
                }
            }
            xmlhttp.open('GET',thefile,true);
            xmlhttp.send();
            xmlhttp.close();
        }

    </script>
</head>
<body>
<div class="container" id="contain">

    <div class="jumbotron" id="OrgSummary">
        <h2></h2>


    </div>

    <form class="navbar-form navbar-right">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>
    <div class="page-header" id="Mmessage">
    <?php echo Message::message();  ?>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-lg-3" >
            <div class="well" id="navbar">
                <ul class="nav nav-pills nav-stacked" role="tablist" >
                    <li ><a href="../BookTitle/create.php">Book Title</a></li>
                    <li><a href="../Birthdate/create.php">Birthday</a></li>
                    <li><a href="../City/create.php">City</a></li>
                    <li><a href="../Email/create.php">Email</a></li>
                    <li><a href="../Gender/create.php">Gender</a></li>
                    <li><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li><a href="../Profile_Picture/create.php">Profile Picture</a></li>
                    <li class="active"><a href="create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-9 col-lg-9 col-sm-9" >
            <div class="panel panel-default">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-info"  data-toggle="modal" data-target="#myModalNorm">
                            <span class="glyphicon glyphicon-plus">create new</span></button>
                        <a href="trashed.php"> <button type="button" class="btn btn-info"  >
                                <span class="glyphicon glyphicon-trash">Trashed List</span></button></a>
                        <div class="nav navbar-right">
                            <label >booklist per page</label>
                            <select  class=" bg-primary"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
                                <?php
                                if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >3 </option>';
                                else echo '<option  value="?ItemsPerPage=3"> 3 </option>';

                                if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >4 </option>';
                                else  echo '<option  value="?ItemsPerPage=4"> 4 </option>';

                                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected > 5 </option>';
                                else echo '<option  value="?ItemsPerPage=5"> 5 </option>';

                                if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected > 6 </option>';
                                else echo '<option  value="?ItemsPerPage=6">6 </option>';

                                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected > 10 </option>';
                                else echo '<option  value="?ItemsPerPage=10"> 10 </option>';

                                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected > 15 </option>';
                                else    echo '<option  value="?ItemsPerPage=15">15 </option>';
                                ?>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="well col-lg-12 col-md-12 col-sm-12" id="navbar">

                        <table class="table table-responsive col-sm-10 col-lg-10">

                            <thead>
                            <tr class="bg-primary">
                                <th style="width:7%">SL.no</th>
                                <th style="width:7%">ID</th>
                                <th style="width:20%">Organization Name</th>

                                <th style="width:30%">Organization Summary</th>

                                <th style="width:36%">Action</th>
                            </tr>
                            </thead>
                            <?php   foreach($someData as $onedata){
                                $limit=100;?>
                               <tbody>
                                <tr>
                               <td ><?php echo $serial;?></td>
                                <td><?php echo $onedata->id;?></td>
                                <td><?php echo $onedata->Org_Name;?></td>
                                <td ><?php $des=$onedata->Org_Summary;
                                        echo substr( $des,0,$limit);
                                         echo "<a href=view.php?>read more</a>";?>
                                             </td>
                                    <?php
                                      echo "<td>";
                                //echo "<ul class='breadcrumb '>";
                                echo "<a ><button class='btn btn-info' data-toggle='modal' data-target='#myModalnorm'
                                               onclick=\"load('Mmessage','view.php?id=$onedata->id');\">view</button> </a>";
                              //  echo "<a ><button class='btn btn-primary' data-toggle='modal' data-target='#MyModalnorm'

                                echo "<a href='edit.php?id=$onedata->id'><button class='btn btn-primary'>edit</button> </a>"; //               onclick=\"load('Mmessage','editt.php?id=$onedata->id');\">edit</button> </a>";
                                    echo "<a href='trash.php?id=$onedata->id'><button class='btn btn-success'>Trash</button></a> ";
                                    echo "<a href='delete.php?id=$onedata->id'><button class='btn btn-danger'>delete</button> </a>";
                                echo "</td>";

                                echo "</tr>";

                                $serial++;



                                // echo  "</ul>";
                                echo "</tbody>";
                            }
                            ?>

                        </table>
                    </div>
                        </div>
                    <nav aria-label="..." class="nav navbar-right">
                        <ul class="pagination">


                            <?php
                            $pre= $page-1;
                            $next=$page+1;
                            if($page==1)
                                echo "<li><a href=''>". 'Previous' . '</a></li>';
                            else
                                echo "<li><a href='?Page=$pre'>". 'Previous' . '</a></li>';
                            for($i=1;$i<=$pages;$i++)
                            {
                                if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                                else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                            }
                            if($page==$pages)
                                echo "<li><a href=''>". 'Next' . '</a></li>';
                            else
                                echo "<li><a href='?Page=$next'>". 'Next' . '</a></li>';
                            ?>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
        <div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h3>Please enter Book Title and Author Name
                            <i class="fa fa-building fa-lg " aria-hidden="true"></i>
                        </h3>

                        <button type="button" class="close"
                                data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>

                    <!-- Modal Body -->
                    <div class="modal-body">

                        <form action="store.php" id="summaryform" method="post" role="form">
                            <div class="form-group">
                                <label for="org_name">Organization Name</label>
                                <input type="text" class="form-control"name="org_name"
                                       id="OrgName" placeholder="Enter organization name"/>
                            </div>
                            <div class="form-group">
                                <label for="org_summary">Organization Summary</label>
                                <textarea class="form-control" rows="5" cols="10" name="org_summary" ></textarea>
                            </div>
                            <button type="submit" class="btn btn-info">Submit</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>



</body>

</html>